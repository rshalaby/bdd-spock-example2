package net.rsworld.bddspock.onepointloader.domain.worktime;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import net.rsworld.bddspock.onepointloader.domain.shared.ProjectId;

@Value
@Builder
class Task {

 @NonNull
 ProjectId projectId;
 @NonNull
 String activity;
 @NonNull
 String skill;

}
