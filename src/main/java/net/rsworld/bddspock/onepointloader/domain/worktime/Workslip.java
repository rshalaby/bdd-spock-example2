package net.rsworld.bddspock.onepointloader.domain.worktime;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@EqualsAndHashCode
public class Workslip {

  private List<WorklogEntry> entries = new ArrayList<>(10);
  private LocalDate workDate;

  public Workslip(@NonNull LocalDate workDate) {
    this.workDate = workDate;
  }


}
