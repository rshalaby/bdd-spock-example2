package net.rsworld.bddspock.onepointloader.domain.project;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import net.rsworld.bddspock.onepointloader.domain.shared.ProjectId;

@Builder
@EqualsAndHashCode
public class Project {

  private ProjectId projectId;

  public Project(@NonNull ProjectId projectId) {
    this.projectId = projectId;
  }

}
