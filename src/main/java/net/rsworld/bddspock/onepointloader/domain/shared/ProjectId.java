package net.rsworld.bddspock.onepointloader.domain.shared;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class ProjectId {

  @NonNull
  String id;

}
