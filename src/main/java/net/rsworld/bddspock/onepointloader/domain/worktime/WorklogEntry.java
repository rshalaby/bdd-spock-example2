package net.rsworld.bddspock.onepointloader.domain.worktime;

import static java.lang.Math.round;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@EqualsAndHashCode
class WorklogEntry {

  @Getter
  private UUID id;
  @Getter
  private Task task;
  @Getter
  private LocalTime startTime;
  @Getter
  private LocalTime endTime;
  @Getter
  private String description;
  @Getter
  private double billableHours;
  private int totalTimeInSeconds;

  @Builder
  public WorklogEntry(@NonNull UUID id, @NonNull Task task, @NonNull LocalTime startTime, @NonNull LocalTime endTime,
      String description, double billableHours) {
    this.id = id;
    this.task = task;
    this.startTime = startTime;
    this.endTime = endTime;
    this.description = description;
    this.billableHours = billableHours;
    this.totalTimeInSeconds = calculateTotalWorktimeSeconds(startTime, endTime);
  }

  public double getTotalWorktimeInHours() {
    var calc = BigDecimal.valueOf(this.totalTimeInSeconds / 3600d);
    return calc.setScale(2, RoundingMode.HALF_UP).doubleValue();
  }

  public int getTotalWorkTimeInSeconds() {
    return totalTimeInSeconds;
  }

  private int calculateTotalWorktimeSeconds(@NonNull LocalTime start, @NonNull LocalTime end) {
    if (endTime.isBefore(startTime)) {
      throw new IllegalArgumentException("End Time can not be before Start Time");
    }
    return end.toSecondOfDay() - start.toSecondOfDay();
  }

}
