package net.rsworld.bddspock.onepointloader.worktime

import net.rsworld.bddspock.onepointloader.domain.shared.ProjectId
import net.rsworld.bddspock.onepointloader.domain.worktime.Task
import net.rsworld.bddspock.onepointloader.domain.worktime.WorklogEntry
import spock.lang.Specification

import java.time.LocalTime

class WorklogEntrySpec extends Specification {

    def "calculate total hours correctly when created start #startTime, end #endTime equals #totalHours h and equals #totalSeconds s"() {
        given: "a normal work task"
        def task = createNormalWorkTask()

        when: "creating a new worklog entry with startTime and endTime"
        def entry = WorklogEntry.builder()
                .id(UUID.randomUUID())
                .task(task)
                .startTime(startTime)
                .endTime(endTime)
                .build()

        then: "totalHours should match the value as per data table below"
        entry.getTotalWorktimeInHours() == totalHours

        and: "total seconds should be endTime minus startTime Seconds"
        entry.getTotalWorkTimeInSeconds() == totalSeconds

        where:
        startTime            | endTime                   | totalHours   | totalSeconds
        LocalTime.of(11, 15) | LocalTime.of(11, 30)      | 0.25         | totalSeconds(startTime, endTime)
        LocalTime.of(0, 0)   | LocalTime.of(23, 59, 59)  | 24           | totalSeconds(startTime, endTime)
        LocalTime.of(8,0)    | LocalTime.of(15, 55)      | 7.92         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 1)        | 0.02         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 5)        | 0.08         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 10)       | 0.17         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 15)       | 0.25         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 20)       | 0.33         | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 30)       | 0.5          | totalSeconds(startTime, endTime)
        LocalTime.of(0,0)    | LocalTime.of(0, 0)        | 0            | totalSeconds(startTime, endTime)
    }

    def "worklog entry can not be created when start time is after end time"() {
        given: "a normal work task"
        def task = createNormalWorkTask()

        when: "creating a entry with start time after end time"
        WorklogEntry.builder()
                .id(UUID.randomUUID())
                .task(task)
                .startTime(LocalTime.of(11, 30) )
                .endTime(LocalTime.of(10, 30) )
                .build()

        then: "IllegalArgument Exception should be thrown"
        thrown(IllegalArgumentException.class)
    }

    static def createNormalWorkTask() {
        return Task.builder()
                .projectId(new ProjectId("myProject"))
                .activity("myactivity")
                .skill("writing spock test")
                .build()
    }

    static def totalSeconds(LocalTime start, LocalTime end) {
      return  end.toSecondOfDay() - start.toSecondOfDay()
    }
}
