package net.rsworld.bddspock.onepointloader

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest
@ActiveProfiles("test")
class BddSpockExampleSpec extends Specification {

    @Autowired
    ApplicationContext context;

    def "Springframework context will load"() {
        expect: "context to be not null"
        context != null
    }
}
